# Projet Hotel V2

A fournir :


Créer un dossier Fichiers_externes qui contiendra :

- Un fichier ListeChambres_V3.csv avec la description des chambres dans cet ordre séparée par des ; et un type de chambre par ligne:
	* Le type de la chambre
	* Sa taille
	* Ses vues (séparées par des virgules)
	* Son occupation (2 adultes et 2 enfants,...)
	* Son tarif (un nombre réel positif)
	* Le nombre de chambres de ce type (un nombre entier positif)
	* Ses options (séparés par des |)

- Un fichier .txt avec le mot de passe admin a la première ligne pour les options réserver une chambre, libérer une chambre, modifier une réservation et annuler une réservation. 


Créer un dossier nommé CreationsOperations.


A savoir :

- Les logins des employés doivent tous commencer par GH avec 3 chiffres derrière (ex: "GH000"). Le login par défaut est GH000.

- Pour executer le programme dans l'invite de commande :
java -jar (nom du jar)
package fr.afpa.utilitaire;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class EcritureFichier {

	/**
	 * Ecrit dans un fichier dont le chemin est passe en parametre le tableau
	 * de chaines de caracteres "contenu".
	 * @param chemin
	 * @param contenu
	 */
	public static void transactionBancaire(String chemin, String[] contenu) {
		try {
			FileWriter fw = new FileWriter(chemin, true);
			BufferedWriter bw = new BufferedWriter(fw);
			for (int i=0; i<contenu.length; i++) {
				bw.write(contenu[i]);
				bw.newLine();
			}
			bw.newLine();
			bw.close();
		} catch (IOException e) {
			System.out.println("Le dossier specifie n'existe pas.");
		}
	}
	
}

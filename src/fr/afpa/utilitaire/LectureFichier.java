package fr.afpa.utilitaire;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import fr.afpa.objets.TypeChambre;

public class LectureFichier {
	
	/**
	 * Permet de lire le fichier contenant le mot de passe et retourne celui-ci.
	 * @param chemin
	 * @return
	 */
	public static String lireMDP(String chemin) {
		try {
			FileReader fr = new FileReader(chemin);
			BufferedReader br = new BufferedReader(fr);
			String res = "";
			while (br.ready()) {
				res = br.readLine();
			}
			br.close();
			return res;
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier n'existe pas.");
		} catch (IOException e) {
			System.out.println("Erreur d'entree/sortie.");
		}
		return null;
	}
	
	/**
	 * Lit un fichier contenant les informations sur les 
	 * differents types de chambres et en retourne un tableau.
	 * @param chemin
	 * @return
	 */
	public static TypeChambre[] traductionFichier(String chemin) {
		String[] texte = lireFichier(chemin);
		int n = texte.length;
		TypeChambre[] cTC = new TypeChambre[n];
		for (int i=0; i<n; i++) {
			String[] ligne = separationUneLigne(texte[i]);
			cTC[i] = traductionUneLigne(ligne);
		}
		return cTC;
	}
	
	/**
	 * Retourne un objet de type CapaciteTypeChambre a partir des
	 * des donnes du tableau de chaine caracteres passe en parametre. 
	 * @param ligne
	 * @return
	 */
	public static TypeChambre traductionUneLigne(String[] ligne) {
		String nom = ligne[0];
		int superficie = traductionSuperficie(ligne[1]);
		String[] vues = separationVues(ligne[2]);
		String occupation = ligne[3];
		float tarif = traductionTarif(ligne[4]);
		int max = traductionNombre(ligne[5]);
		String[] options = separationOptions(ligne[6]);
		return new TypeChambre(max, nom, occupation, vues, options, superficie, tarif);
	}
	
	/**
	 * Separe les informations d'une chaine de caracteres par ";" et en
	 * retourne un tableau
	 * @param ligne
	 * @return
	 */
	public static String[] separationUneLigne(String ligne) {
		return ligne.split(";");
	}
	
	/**
	 * Transforme une superficie de type chaine de caracteres en
	 * un entier
	 * @param taille
	 * @return
	 */
	private static int traductionSuperficie(String taille) {
		try {
			String temp = "";
			int i=0;
			while (i<taille.length() && Character.isDigit(taille.charAt(i))) {
				temp += taille.charAt(i);
				i++;
			}
			return Integer.parseInt(temp);
		}
		catch (NumberFormatException e) {
			System.out.println("C'est pas un entier.");
		}
		return -1;
	}
	
	/**
	 * Separe une chaine de caracteres par des "," et retourne un tableau
	 * de chaine de caracteres contenant une vue par case.
	 * @param ligne
	 * @return
	 */
	private static String[] separationVues(String ligne) {
		return ligne.split(",");
	}
	
	/**
	 * Transforme un tarif dans une chaine de caracteres en un tarif
	 * de type float.
	 * @param ligne
	 * @return
	 */
	private static float traductionTarif(String ligne) {
		try {
			return Float.parseFloat(ligne);
		}
		catch (NumberFormatException e) {
			System.out.println("C'est pas un flottant.");
		}
		return -1f;
	}
	
	/**
	 * Transforme le nombre total d'un type de chambre qui etait
	 * en chaine de caracteres en un entier.
	 * @param ligne
	 * @return
	 */
	private static int traductionNombre(String ligne) {
		return Integer.parseInt(ligne);
	}
	
	/**
	 * Separe les options par rapport a "|" et retourne un
	 * tableau de chaines de caracteres d'options.
	 * @param ligne
	 * @return
	 */
	public static String[] separationOptions(String ligne) {
		return ligne.split("\\|");
	}
	
	/**
	 * Lit un fichier a partir de son chemin et retourne un tableau
	 * de chaines de caracteres pour chaque ligne du fichier,
	 * sans la premiere ligne.
	 * @param cheminFichier
	 * @return
	 */
	public static String[] lireFichier(String cheminFichier) {
		try {
			FileReader fr = new FileReader(cheminFichier);
			BufferedReader br = new BufferedReader(fr);
			String[] res = new String[nbLignesFichier(cheminFichier)-1];
			int i=0;
			while (br.ready()) {
				if (i != 0) {
					res[i-1] = br.readLine();
				}
				else {
					br.readLine();
				}
				i++;
			}
			br.close();
			return res;
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier n'existe pas.");
		} catch (IOException e) {
			System.out.println("Erreur d'entree/sortie.");
		}
		return null;
	}
	
	/**
	 * Compte le nombre de lignes du fichier dont le chemin
	 * est passe en parametre.
	 * @param cheminFichier
	 * @return
	 */
	private static int nbLignesFichier(String cheminFichier) {
		try {
			FileReader fr = new FileReader(cheminFichier);
			BufferedReader br = new BufferedReader(fr);
			int i=0;
			while (br.ready()) {
				br.readLine();
				i++;
			}
			br.close();
			return i;
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier n'existe pas.");
		} catch (IOException e) {
			System.out.println("Erreur d'entree/sortie.");
		}
		return -1;
	}
	
	
}

package fr.afpa.utilitaire;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class ControleSaisie {

	/**
	 * Verifie si le login d'un client passe en parametre est 
	 * est valide.
	 * @param login
	 * @return
	 */
	public static boolean isStringLoginClientValide(String login) {
		if (login.length()==10) {
			int i=0;
			i = testDigit(login, i);
			return i==10;
		}
		return false;
	}
	
	/**
	 * Verifie si le login d'un employe passe en parametre
	 * est valide.
	 * @param login
	 * @return
	 */
	public static boolean isStringLoginEmployeValide(String login) {
		if (login.length()==5 && login.substring(0,2).equals("GH")) {
			int i = 2;
			i = testDigit(login, i);
			return i==5;
		}
		return false;
	}
	
	/**
	 * Verifie si l'adresse mail passee en parametre est valide.
	 * @param adresse
	 * @return
	 */
	public static boolean isStringMailValide(String adresse) {
		String[] resultArobase = adresse.split("@");
		if (resultArobase.length!=2 || resultArobase[0].length()==0 || resultArobase[1].length()==0) {
			return false;
		}
		String[] resultPoint = resultArobase[1].split("\\.");
		if (resultPoint.length!=2 || resultPoint[0].length()==0 || resultPoint[1].length()==0) {
			return false;
		}
		return true;
	}
	
	/**
	 * Incremente i tant que le caractere a l'indice i de nom
	 * n'est pas le caractere c.
	 * @param nom
	 * @param i
	 */
	private static int testNom(String nom, int i, char c) {
		while (i<nom.length() && nom.charAt(i) != c) {
			i++;
		}
		return i;
	}
	
	/**
	 * Verifie si la chaine de caractere passee en parametre
	 * represente une date valide ou non.
	 * @param date
	 * @return
	 */
	public static boolean isStringDateValide(String str) {
		String[] strDate = str.split("/"); 
		if (strDate.length==3) {
		try {
			LocalDate.parse(strDate[2]+"-"+strDate[1]+"-"+strDate[0]);
			return true;
		}
		catch (DateTimeParseException e) {
			return false;
		}
		}
		else return false;
	}
	
	/**
	 * Verifie si la date de debut du sejour est avant sa date de fin.
	 * Les dates de debut et de fin sont passees en parametres.
	 * @param debut
	 * @param fin
	 * @return
	 */
	public static boolean isDureeSejourValide(LocalDate debut,LocalDate fin) {
		return 0<fin.compareTo(debut);
	}
	
	/**
	 * Verifie si "date" est une date qui ne se trouve pas entre les dates "debut" et "fin".
	 * @param debut
	 * @param fin
	 * @param date
	 * @return
	 */
	public static boolean isPasDansZoneReservation(LocalDate debut, LocalDate fin, LocalDate date) {
		return !(debut.compareTo(date)<=0&&fin.compareTo(date)>=0);
	}
	
	/**
	 * Verifie si la chaine de caractere passee en parametre
	 * represente un entier.
	 * @param nombre
	 * @return
	 */
	public static boolean isStringEntier(String nombre) {
		try {
			Integer.parseInt(nombre);
			return true;
		}
		catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * Verifie si la chaine de caractere passee en parametre
	 * est egal a "oui" ou "non".
	 * @param booleen
	 * @return
	 */
	public static boolean isStringBoolean(String booleen) {
		return booleen.equalsIgnoreCase("oui") || booleen.equalsIgnoreCase("non");
	}
	
	/**
	 * Verifie si la chaine de caractere passee en parametre
	 * represente un nombre decimal.
	 * @param decimal
	 * @return
	 */
	public static boolean isStringFloat(String decimal) {
		try {
			Float.parseFloat(decimal);
			return true;
		}
		catch (NumberFormatException e1) {
			return false;
		}
		catch (NullPointerException e2) {
			return false;
		}
		
		
	}
	
	/**
	 * Incremente i tant que le caractere a l'indice i de nb
	 * est un chiffre.
	 * @param nb
	 * @param i
	 */
	public static int testDigit(String nb, int i) {
		while (i<nb.length() && Character.isDigit(nb.charAt(i))) {
			i++;
		}
		return i;
	}
}

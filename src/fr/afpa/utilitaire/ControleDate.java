package fr.afpa.utilitaire;

public class ControleDate {
	
	/**
	 * Permet de verifier si une chaine de caracteres passee
	 * en parametre est une date valide.
	 * @param date
	 * @return
	 */
	protected static boolean verifDateValide(String date) {
		String[] separation = separationDate(date);
		if (!verifLongueurDate(separation)) {
			return false;
		}
		int[] res = convertionTableauNombre(separation);
		int jour = res[0];
		int mois = res[1];
		int annee = res[2];
		if (verifAnneeBissex(annee)) {
			return verifMois29(jour, mois)
				|| verifMois30(jour, mois)
				|| verifMois31(jour, mois);
		}
		else {
			return verifMois28(jour, mois)
				|| verifMois30(jour, mois)
				|| verifMois31(jour, mois);
		}
	}
	
	/**
	 * Verifie si l'anne passee en parametre est bissextile.
	 * @param annee
	 * @return
	 */
	private static boolean verifAnneeBissex(int annee) {
		return (annee%4==0) && (annee%100!=0) && (annee%400==0);
	}
	
	/**
	 * Verifie si pour une annee bissextile, le jour du mois
	 * de fevrier est valide si ce mois est fevrier.
	 * @param jour
	 * @param mois
	 * @return
	 */
	private static boolean verifMois29(int jour, int mois) {
		return jourValide(jour, 29) && (mois==2);
	}
	
	/**
	 * Verifie si pour une annee non bissextile, le jour du mois
	 * de fevrier est valide si ce mois est fevrier.
	 * @param jour
	 * @param mois
	 * @return
	 */
	private static boolean verifMois28(int jour, int mois) {
		return jourValide(jour, 28) && (mois==2);
	}
	
	/**
	 * Verifie si le jour des mois a 31 jours est valide et si ce
	 * mois a bel et bien 31 jours.
	 * @param jour
	 * @param mois
	 * @return
	 */
	private static boolean verifMois31(int jour, int mois) {
		return jourValide(jour, 31) 
			&& (mois==1 || mois==3 || mois==5 || mois==7
				|| mois==8 || mois==10 || mois==12);
	}
	
	/**
	 * Verifie si le jour des mois a 30 jours est valide et si ce
	 * mois a bel et bien 30 jours.
	 * @param jour
	 * @param mois
	 * @return
	 */
	private static boolean verifMois30(int jour, int mois) {
		return jourValide(jour, 30) && (mois==4 || mois==6 || mois==9 || mois==11);
	}
	
	/**
	 * Verifie si le jour est compris entre 1 et le jourMax.
	 * @param jour
	 * @param jourMax
	 * @return
	 */
	private static boolean jourValide(int jour, int jourMax) {
		return 1<=jour && jour<=jourMax;
	}
	
	/**
	 * Verifie si le mois est compris entre 1 et 12.
	 * @param mois
	 * @return
	 */
	private static boolean moisValide(int mois) {
		return 1<=mois && mois <=12;
	}

	/**
	 * Tranforme un tableau de date en chaines de caracteres en un
	 * tableau de date en entiers.
	 * @param tab
	 * @return
	 */
	private static int[] convertionTableauNombre(String[] tab ) {
		int[] res = new int[tab.length];
		for (int i=0; i<res.length; i++) {
			if (ControleSaisie.isStringEntier(tab[i])) {
				res[i] = Integer.parseInt(tab[i]);
			}
		}
		return res;
	}
	
	/**
	 * Conertit une date sos forme de chaine de caracteres en un
	 * un tableau d'entiers : [jour|mois|annee].
	 * @param date
	 * @return
	 */
	public static int[] conversionDateStringToTabInt(String date) {
		String[] temp = separationDate(date);
		return convertionTableauNombre(temp);
	}
	
	/**
	 * Verifie si le tableau de date a bien la longueur d'un tableau
	 * compose d'un jour, un mois et d'une annee.
	 * @param date
	 * @return
	 */
	private static boolean verifLongueurDate(String[] date) {
		return date.length == 3;
	}
	
	/**
	 * Separe une date en chaines de caracteres separees par "/"
	 * en un tableau de chaines de caracteres [jour|mois|annee].
	 * @param date
	 * @return
	 */
	private static String[] separationDate(String date) {
		return date.split("/");
	}

	
			
}
	

package fr.afpa.testsObjets;

import java.time.LocalDate;

import fr.afpa.objets.Chambre;
import fr.afpa.objets.Client;
import fr.afpa.objets.Reservation;
import fr.afpa.objets.TypeChambre;

public class TestChambre {

	public static void main(String[] args) {
		
		String[] vues = {"Vue 1", "Vue 2", "Vue 3"};
		String[] options = {"Option 1", "Option 2", "Option 3"};
		
		TypeChambre type = new TypeChambre(6, "Nom", "Occupation", vues, options, 100, 50.0f);
		
		Chambre chambre = new Chambre(type);
		
		testCoutReservationsSansReservation(chambre);
		System.out.println();
		
		Client client = new Client("Nom", "Prenom", "mail");
		chambre.getReservations()[0] = new Reservation("login 1", client, LocalDate.of(1994, 03, 24), LocalDate.of(1994, 03, 27));
		chambre.getReservations()[3] = new Reservation("login 2", client, LocalDate.of(1994, 02, 24), LocalDate.of(1994, 02, 26));
		
		testCoutReservationsAvecReservation(chambre);
		System.out.println();
		
		testChambreLibre(chambre);
		System.out.println();
		
		chambre.getReservations()[1] = new Reservation("login 3", client, LocalDate.of(1994, 03, 24), LocalDate.of(1994, 03, 27));
		chambre.getReservations()[2] = new Reservation("login 4", client, LocalDate.of(1994, 02, 24), LocalDate.of(1994, 02, 26));
		chambre.getReservations()[4] = new Reservation("login 5", client, LocalDate.of(1994, 02, 24), LocalDate.of(1994, 02, 26));
		
		testChambreReservee(chambre);

	}
	
	public static void testCoutReservationsSansReservation(Chambre chambre) {
		System.out.println("Calcul couts de la chambre :");
		float[] couts = chambre.calculCoutSejours();
		for (int i=0; i<couts.length; i++) {
			System.out.println("Reservation "+(i+1)+" : "+couts[i]);
		}
	}
	
	public static void testCoutReservationsAvecReservation(Chambre chambre) {
		System.out.println("Calcul couts de la chambre :");
		float[] couts = chambre.calculCoutSejours();
		for (int i=0; i<couts.length; i++) {
			System.out.println("Reservation "+(i+1)+" : "+couts[i]);
		}
	}
	
	public static void testChambreLibre(Chambre chambre) {
		System.out.println("Une reservation possible : "+chambre.chambreLibre());
	}
	
	public static void testChambreReservee(Chambre chambre) {
		System.out.println("Pas de reservation possible : "+!chambre.chambreLibre());
	}

}

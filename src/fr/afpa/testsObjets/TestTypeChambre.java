package fr.afpa.testsObjets;

import fr.afpa.objets.TypeChambre;

public class TestTypeChambre {

	public static void main(String[] args) {
		
		String[] vues = {"Vue 1", "Vue 2", "Vue 3"};
		String[] options = {"Option 1", "Option 2", "Option 3"};
		
		TypeChambre type = new TypeChambre(6, "Nom", "Occupation", vues, options, 100, 50.0f);

		type.infos();
	}

	
	
	
}

package fr.afpa.testsObjets;

import java.time.LocalDate;

import fr.afpa.objets.Client;
import fr.afpa.objets.Reservation;

public class TestReservation {

	public static void main(String[] args) {
		
		Client client = new Client("WAYNE", "Bruce", "mail");
		Reservation reservation = new Reservation("123456789", client, LocalDate.of(1994, 03, 24), LocalDate.of(1994, 03, 27));

		System.out.println("Duree sejour est egale a 3 : "+(reservation.calculDureeSejour()==3));
		
		reservation.createInfosPdf("TestPDF\\", 3);
		
	}
	

}

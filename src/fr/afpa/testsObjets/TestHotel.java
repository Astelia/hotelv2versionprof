package fr.afpa.testsObjets;

import fr.afpa.objets.Chambre;
import fr.afpa.objets.Hotel;
import fr.afpa.objets.TypeChambre;

public class TestHotel {

	public static void main(String[] args) {
		
		/*String[] vues = {"Vue 1", "Vue 2", "Vue 3"};
		String[] options = {"Option 1", "Option 2", "Option 3"};
		
		TypeChambre type = new TypeChambre(6, "Nom", "Occupation", vues, options, 100, 50.0f);*/

		Hotel hotel = new Hotel("Truc");
		/*for (int i=0; i<hotel.getChambres().length; i++) {
			System.out.println("\nChambre numero "+(i+1));
			hotel.getChambres()[i].getType().infos();
		}*/
		
		int[] indices = hotel.trouverIndicesChambresParTypeChambre(hotel.getTypesChambres()[0]);
		for (int i=0; i<indices.length; i++) {
			System.out.println(indices[i]);
		}
		System.out.println();
		
		boolean[] dispo = hotel.disponibiliteTypeChambres();
		for (int i=0; i<dispo.length; i++) {
			System.out.println(i+" - > "+dispo[i]);
		}
		
	}

}

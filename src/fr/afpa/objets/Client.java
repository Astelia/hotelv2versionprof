package fr.afpa.objets;

public class Client {

	private String nom;
	private String prenom;
	private String mail;
	
	
	public Client(String nom_, String prenom_, String mail_) {
		nom = nom_;
		prenom = prenom_;
		mail = mail_;
	}

	
	/**
	 * Retourne un tableau des infos du client sans son numero de carte
	 * @param chambres
	 * @return
	 */
	public String infosClient() {
		String res = "Nom : "+nom+" "+prenom;
		res += "\nEmail : "+mail;
		return res;
	}
	
	/**
	 * Affichage des infos du client
	 * @param chambres
	 */
	public void afficherInfos() {
		System.out.println(infosClient());
	}
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom_) {
		nom = nom_;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom_) {
		prenom = prenom_;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail_) {
		mail = mail_;
	}
	
	
	
}

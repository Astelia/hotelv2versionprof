package fr.afpa.objets;

import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import fr.afpa.utilitaire.ControleSaisie;
import fr.afpa.utilitaire.EnvoiMail;
import fr.afpa.utilitaire.LectureFichier;
import fr.afpa.utilitaire.Operation;
import fr.afpa.utilitaire.Question;

public class Hotel {

	private String nom;
	private LocalDate dateDuJour;
	private Chambre[] chambres;
	private TypeChambre[] typesChambres;
	private String[] employes;
	private Scanner in;
	
	
	public Hotel(String nom_) {
		nom = nom_;
		dateDuJour = LocalDate.now();
		
		String fileName = "Fichiers_externes\\ListeChambres_V3.csv";
		typesChambres = LectureFichier.traductionFichier(fileName);
		chambres = creationHotel();
		
		employes = new String[1];
		employes[0] = "GH000";
		in = new Scanner(System.in);
		
		System.out.println("Bienvenue a l'hotel "+nom+"\n");
		menuAuthentif();
		in.close();
	}

	
	/**
	 * Affichage du menu de l'hotel
	 */
	public void affichageMenuHotel() {
		System.out.println("-------------------  MENU HOTEL CDA JAVA -----------------------");
		System.out.println();
		System.out.println("A- Afficher l'etat de l'hotel");
		System.out.println("B- Afficher le nombre de chambre reservees");
		System.out.println("C- Afficher le nombre de chambre libres");
		System.out.println("D- Afficher le numero de la premiere chambre vide");
		System.out.println("E- Afficher le numero de la derniere chambre vide");
		System.out.println("F- Reserver une chambre");
		System.out.println("G- Liberer une chambre");
		System.out.println("H- Modifier une reservation");
		System.out.println("I- Annuler une reservation");
		System.out.println();
		System.out.println("Q- Quitter");
		System.out.println();
		System.out.println("-----------------------------------------------------------------");
		System.out.println();
		System.out.print("Votre choix : ");
	}
	
	/**
	 * Menu de l'hotel qui appelle les fonctions associees
	 */
	public void menuHotel() {
		affichageMenuHotel();
		String reponse = Question.scannerMot(in);
		switch (reponse) {
			case "A" : affichageChambres(listeChambresReservees()
							, "Liste des chambres reservees :");
					   break;
			case "B" : affichageNbChambresReservees();
					   break;
			case "C" : affichageNbChambresLibres();
					   break;
			case "D" : affichagePremiereChambre(true);
					   break;
			case "E" : affichageDerniereChambre(true);
					   break;
			case "F" : {	if (verifMotDePasse()) {
								reserverChambre();
							}
							else {
								System.out.println("Mot de passe invalide !");
							}
						}
					   break;
			case "G" : liberation();
				   	   break;
			case "H" : modification();
				   	   break;
			case "I" : {	if (verifMotDePasse()) {
								annulerReservation();
							}
							else {
								System.out.println("Mot de passe invalide !");
							}
						}
				   	   break;
		}
		quitter(reponse);
	}
	
	/**
	 * Appelle la fonction menuHotel si la reponse n'est pas "Q"
	 * et met fin au programme sinon.
	 * @param reponse
	 */
	public void quitter(String reponse) {
		if (!reponse.equals("Q")) {
			menuAuthentif();
		}
	}
	
	/**
	 * Verifie si le mot de passe entre dans la console est correct.
	 * @return
	 */
	public boolean verifMotDePasse() {
		System.out.print("Veuillez entrer le mot de passe : ");
		String mdp = Question.scannerMot(in);
		return mdp.equals(LectureFichier.lireMDP("Fichiers_externes\\MotDePasse.txt"));
	}
	
	/**
	 * Affichage menu authentification
	 */
	public static void affichageMenuAuthentif() {
		System.out.println("_________________________________________________");
		System.out.println();
		System.out.println("             Entrer votre login                  ");
		System.out.println();
		System.out.println("_________________________________________________");
	}
	
	/**
	 * Permet a l'utilisateur de s'authentifier.
	 * Affiche le menu en boucle si login non valide ou si
	 * le client ou l'employe n'existent pas.
	 */
	public void menuAuthentif() {
		affichageMenuAuthentif();
		String login = Question.scannerMot(in);
		if (ControleSaisie.isStringLoginClientValide(login) && reservationExistParLogin(login)) {
			int indiceChambre = trouverIndiceChambreParLogin(login);
			System.out.println("Chambre "+(indiceChambre+1));
			Chambre chambre = chambres[indiceChambre];
			Reservation reserv = chambre.getReservations()[trouverIndiceReservationParLogin(login)];
			reserv.infos();
			try {
				// Permet au programme d'attendre 20 secondes avant de faire l'instruction suivante
				TimeUnit.SECONDS.sleep(20L);
			} catch (InterruptedException e) {
				System.out.println("Impossible d'attendre !");
			}
			menuAuthentif();
		}
		else if (ControleSaisie.isStringLoginEmployeValide(login) && employeExistParLogin(login)) {
			menuHotel();
		}
		else {
			System.out.println("Login incorrect !");
			menuAuthentif();
		}
	}
	
	/**
	 * Verifie si l'employe existe dans le tableau des employes
	 * @param login
	 * @return
	 */
	public boolean employeExistParLogin(String login) {
		int i=0;
		while (i<employes.length && !employes[i].contentEquals(login)) {
			i++;
		}
		return i != employes.length;
	}
	
	/**
	 * ajoute un employe dans le tableau des employes
	 * s'il n'existe pas.
	 * @param login
	 */
	public void ajouterEmploye(String login) {
		if (!employeExistParLogin(login)) {
			nouveauTableauEmployes();
			employes[employes.length-1] = login;
		}
		else {
			System.out.println("L'employe existe deja !");
		}
	}
	
	/**
	 * Remplace l'ancien tableau d'employes avec un nouveau
	 * tableau d'employes avec les memes valeurs mais, une case en plus.
	 */
	private void nouveauTableauEmployes() {
		String[] res = new String[employes.length + 1];
		for (int i=0; i<employes.length; i++) {
			res[i] = employes[i];
		}
		employes = res;
	}
	
	/**
	 * Verifie si le login en parametre pour la fonction trouverChambreParLogin retourne
	 * l'indice d'une chambre dans le tableau des chambres.
	 * @param login
	 * @return
	 */
	public boolean chambreExistParLogin(String login) {
		return trouverIndiceChambreParLogin(login) != -1;
	}
	
	/**
	 * Compare le login entre en parametre avec les logins de chaque reservation dans 
	 * chaque chambre et retourne l'indice de la chambre ou se trouve la reservation associee.
	 * @param login
	 * @return
	 */
	public int trouverIndiceChambreParLogin(String login) {
		int i=0;
		while (i<chambres.length) {
			int j=0;		
			while (j<chambres[i].getReservations().length) {
				if (chambres[i].getReservations()[j]!=null && chambres[i].getReservations()[j].getLogin().equals(login)) {
					return i;
				}
				j++;
			}
			i++;
		}
		return -1;
	}
	
	/**
	 * Verifie si le login pointe une reservation qui se trouve dans l'une des chambres 
	 * du tableau de chambres.
	 * @param login
	 * @return
	 */
	public boolean reservationExistParLogin(String login) {
		return trouverIndiceReservationParLogin(login) != -1;
	}
	
	/**
	 * Compare le login entre en parametre avec les logins de chaque reservation dans 
	 * chaque chambre et retourne l'indice de la reservation associee dans la chambre ou il se trouve.
	 * S'il ne trouve pas, il renvoie -1.
	 * @param login
	 * @return
	 */
	public int trouverIndiceReservationParLogin(String login) {
		int i=0;
		while (i<chambres.length) {
			int j=0;		
			while (j<chambres[i].getReservations().length) {
				if (chambres[i].getReservations()[j]!=null && chambres[i].getReservations()[j].getLogin().equals(login)) {
					return j;
				}
				j++;
			}
			i++;
		}
		return -1;
	}
	
	/**
	 * Permet d'annuler une reservation si celle-ci existe.
	 */
	public void annulerReservation() {
		System.out.print("Veuillez entrer votre login : ");
		String login = Question.scannerMot(in);
		if (reservationExistParLogin(login)) {
			int indiceChambre = trouverIndiceChambreParLogin(login);
			int indiceReservation = trouverIndiceReservationParLogin(login);
			payerFacture(login, false);
			Reservation reservation = chambres[indiceChambre].getReservations()[indiceReservation];
			creationPDFFactureEtEnvoie("CreationsPDFs\\", reservation, indiceChambre);
			liberation();
		}
		else {
			System.out.println("Il n'y a aucune reservation a ce nom.");
		}
	}
	
	/**
	 * Fonction permettant de reserver une chambre
	 */
	public void reserverChambre() {
		if (nbChambresLibres() > 0) {
			System.out.print("Veuillez entrer le nom : ");
			String nom = Question.scannerMot(in);
			System.out.print("Veuillez entrer le prenom : ");
			String prenom = Question.scannerMot(in);
			int indiceTypeChambre = choisirIndiceTypeChambreParCriteres();
			if (!clientExists(nom, prenom) || (nbMaxReservationsParClient(nom, prenom) && verifIndiceChambre(indiceTypeChambre)!=-1)) {		
				System.out.print("Entrer la date du debut du sejour (jj/mm/aaaa) : ");
				LocalDate debut = Question.scannerDate(in);
				System.out.print("Entrer la date du fin du sejour (jj/mm/aaaa) : ");
				LocalDate fin = Question.scannerDate(in);
				if (ControleSaisie.isDureeSejourValide(debut, fin) && ControleSaisie.isDureeSejourValide(dateDuJour, debut)) {
					int indiceChambre = compareDatesSejourDansHotel(debut, fin, indiceTypeChambre);
					if (reservationTrouveeDansChambre(debut, fin, indiceChambre)) {
						int indiceReservation = compareDatesSejourDansChambre(debut, fin, indiceChambre);
						Reservation reservation = creationReservation(nom, prenom, debut, fin);
						chambres[indiceChambre].getReservations()[indiceReservation] = reservation;
						payerFacture(reservation.getLogin(), true);
						creationPDFFactureEtEnvoie("CreationsPDFs\\", reservation, indiceChambre);
						System.out.println("La reservation a ete effectuee.");
					}
					else {
						System.out.println("La reservation est impossible !");
						System.out.println("Il n'y a pas de chambre diponible pour ces dates.");
					}
				}
				else {
					System.out.println("La reservation est impossible !");
					System.out.println("L'une des dates entrees en parametre est anterieure a la date du jour"
							+ " ou la date de debut et la date de fin sont inversees.");
				}
			}
			else {
				System.out.println("Vous avez deja 5 reservations dans notre hotel !");
			}
		}
		else {
			System.out.println("L'hotel n'a plus de chambre disponible !");
		}
	}
	
	/**
	 * Prend en parametre deux dates pour la reservation et retourne l'indice de la chambre ou 
	 * l'on peut effectuer la reservation selon les criteres choisis par le client.
	 * @param debut
	 * @param fin
	 * @return
	 */
	public int compareDatesSejourDansHotel(LocalDate debut, LocalDate fin, int indiceTypeChambre) {
		int indiceChambre = 0;
		for (int i=0; i<indiceTypeChambre; i++) {
			indiceChambre += typesChambres[i].getNbExemplaires();
		}
		int indiceMax = indiceChambre + chambres[indiceChambre].getType().getNbExemplaires();
		while (indiceChambre<indiceMax) {
			if (reservationTrouveeDansChambre(debut, fin, indiceChambre)) {
				return indiceChambre;
			}
			indiceChambre++;
		}
		return -1;
	}
	
	/**
	 * Verifie que les dates de sejour n'entrent pas en conflit avec les autres dates
	 * de reservations de la chambre.
	 * @param debut
	 * @param fin
	 * @param indiceChambre
	 * @return
	 */
	public boolean reservationTrouveeDansChambre(LocalDate debut, LocalDate fin, int indiceChambre) {
		return compareDatesSejourDansChambre(debut, fin, indiceChambre) != -1;
	}
	
	/**
	 * Cherche a l'indice de la chambre passee en parametre une reservation qui n'entre pas en conflit avec
	 * les autres reservations de la chambre et retourne son indice.
	 * S'il ne trouve pas, il retourne -1.
	 * @param debut
	 * @param fin
	 * @param indiceChambre
	 * @return
	 */
	public int compareDatesSejourDansChambre(LocalDate debut, LocalDate fin, int indiceChambre) {
		int i=0;
		int cpt = -1;
		Reservation[] reservations = chambres[indiceChambre].getReservations();
		while (i<reservations.length) {
			if (reservations[i] == null) {
				cpt = i;
			}
			else {
				if (!ControleSaisie.isPasDansZoneReservation(reservations[i].getDebut(), reservations[i].getFin(), debut)
						|| !ControleSaisie.isPasDansZoneReservation(reservations[i].getDebut(), reservations[i].getFin(), fin)) {
					return -1;
				}
			}
			i++;
		}
		return cpt;
	}
	
	
	
	
	/**
	 * Permet de creer une facture en pdf avec un calcul du montant a payer.
	 * credit pour l'hotel = true, debit pour l'hotel = false
	 * @param login
	 * @param debit_credit
	 */
	public void payerFacture(String login, boolean debit_credit) {
		int indiceChambre = trouverIndiceChambreParLogin(login);
		Chambre chambre = chambres[indiceChambre];
		int indiceReservation = trouverIndiceReservationParLogin(login);
		float sommeAPayer = chambre.calculCoutSejours()[indiceReservation];
		if (debit_credit) {
			System.out.println("Montant a payer "+sommeAPayer+" euros.");
		}
		else {
			System.out.println("Montant du remboursement : "+sommeAPayer+" euros.");
		}
		
		System.out.print("Veuillez entrer le numero de votre carte : ");
		String numeroCarte = Question.scannerMot(in);
		Operation operation = new Operation(dateDuJour, debit_credit, sommeAPayer, numeroCarte);
		String dossier = "CreationsOperations\\";
		operation.fichierOperation(dossier);
		Reservation reservation = chambre.getReservations()[indiceReservation];
		creationPDFFactureEtEnvoie(dossier, reservation, indiceChambre);
	}
	
	/**
	 * Creation du pdf avec l'envoie de mail
	 * @param dossier
	 * @param reservation
	 * @param indiceChambre
	 */
	public void creationPDFFactureEtEnvoie(String dossier, Reservation reservation, int indiceChambre) {
		String fileName = reservation.createInfosPdf(dossier, indiceChambre);
		EnvoiMail.envoyerMailSMTP(fileName, reservation.getClient().getMail());
	}
	
	/**
	 * Cree un client avec le nom et le prenom passes en parametre.
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public Reservation creationReservation(String nom, String prenom, LocalDate debut, LocalDate fin) {
		System.out.print("Veuillez entrer votre mail : ");
		String mail = Question.scannerMail(in);
		Client client = new Client(nom, prenom, mail);
		return new Reservation(genererLogin(), client, debut, fin);
	}
	 
	/**
	 * Genere un login et verifie qu'aucun autre client n'a le meme, si oui, regenere un autre
	 * si non, le retourne.
	 * @return
	 */
	public String genererLogin() {
		String login = "";
		for (int i=0; i<10; i++) {
			login += aleatoireInferieur10();
		}
		if (loginPasUtilise(login)) {
			return login;
		}
		else {
			return genererLogin();
		}
	}
	
	/**
	 * Verifie si le login passe en parametre n'a pas deja ete utilise.
	 * @param login
	 * @return
	 */
	public boolean loginPasUtilise(String login) {
		int i=0;
		while (i<chambres.length) {
			int j=0;
			Reservation[] reserv = chambres[i].getReservations();
			while (j<reserv.length) {
				if (reserv[j] != null && reserv[j].getLogin().equals(login)) {
					return false;
				}
				j++;
			}
			i++;
		}
		return true;
	}
	
	/**
	 * Genere un nombre entier aleatoire inferieur a 10.
	 * @return
	 */
	public int aleatoireInferieur10() {
		return new Random().nextInt(10);
	}
	
	/**
	 * Permet de verifier si l'indice de la chambre passe en parametre est bien l'indice de la chambre ou se trouve la reservation 
	 * @param numerosChambres
	 * @param numATester
	 * @return
	 */
	public boolean indiceChambreAppartientAReservationClient(int[] numerosChambres, int numATester) {
		int i=0;
		while (i<numerosChambres.length) {
			if (numerosChambres[i] == numATester) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Permet de verifier si le client a bien une reservation sous ce nom et ce prenom.
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public boolean clientExists(String nom, String prenom) {
		int[] indicesChambres = trouverIndicesChambresParNomPrenom(nom, prenom);
		int nbReservations = 0;
		for (int i=0; i<indicesChambres.length; i++) {
			if (indicesChambres[i] != -1) {
				nbReservations++;
			}
		}
		return nbReservations > 0;
	}
	
	/**
	 * Permet de verifier si le client a moins de 5 reservations dans l'hotel.
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public boolean nbMaxReservationsParClient(String nom, String prenom) {
		int[] indicesChambres = trouverIndicesChambresParNomPrenom(nom, prenom);
		int nbReservations = 0;
		for (int i=0; i<indicesChambres.length; i++) {
			if (indicesChambres[i] != -1) {
				nbReservations++;
			}
		}
		return nbReservations < 5; 
	}
	
	/**
	 * Permet de retourner les indices des chambres qu'a deja reserve le client avec 
	 * en parametre, son nom et son prenom.
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public int[] trouverIndicesChambresParNomPrenom(String nom, String prenom) {
		int[] indicesChambres = new int[5];
		for (int i=0; i<indicesChambres.length; i++) {
			indicesChambres[i] = -1;
		}
		
		int cpt=0;
		int i=0;
		while (i<chambres.length && cpt<5) {
			int j=0;
			Reservation[] reserv = chambres[i].getReservations();
			while (j<reserv.length && cpt<5) {
				if (reserv[j]!=null && reserv[j].getClient().getNom().equals(nom) && reserv[j].getClient().getPrenom().equals(prenom)) {
					indicesChambres[cpt] = i;
					cpt++;
				}
				j++;
			}
			i++;
		}
		return indicesChambres;
	}
	
	/**
	 * Permet de trouver les indices de toutes les reservations prises avec le nom et le prenom passes en parametre.
	 * La fonction cherche dans tout l'hotel.
	 * @param nom
	 * @param prenom
	 * @param indiceChambre
	 * @return
	 */
	public int[] trouverIndicesReservationParNomPrenom(String nom, String prenom) {
		int[] indicesReservations = new int[5];
		for (int i=0; i<indicesReservations.length; i++) {
			indicesReservations[i] = -1;
		}
		int i=0;
		int cpt = 0;
		while (i<chambres.length && cpt<5) {
			int [] reservationsParChambre = trouverIndicesReservationsParNomPrenomIndiceChambre(nom, prenom, i);
			int j=0;
			while (cpt<5 && reservationsParChambre[j] != -1) {
				indicesReservations[cpt] = reservationsParChambre[j];
				cpt++;
			}
			j++;
		}
		return indicesReservations;
	}
	
	/**
	 * Permet de trouver les indices de toutes les reservations prises avec le nom et le prenom passes en parametre
	 * dans la chambre dont l'indice est passe en parametre.
	 * @param nom
	 * @param prenom
	 * @param indiceChambre
	 * @return
	 */
	public int[] trouverIndicesReservationsParNomPrenomIndiceChambre(String nom, String prenom, int indiceChambre) {
		int[] indicesReservations = new int[5];
		for (int i=0; i<indicesReservations.length; i++) {
			indicesReservations[i] = -1;
		}
		int cpt=0;
		int j=0;
		Reservation[] reserv = chambres[indiceChambre].getReservations();
		while (j<reserv.length && cpt<5) {
			if (reserv[j]!=null && reserv[j].getClient().getNom().equals(nom) && reserv[j].getClient().getPrenom().equals(prenom)) {
				indicesReservations[cpt] = j;
				cpt++;
			}
			j++;
		}
		return indicesReservations;
	}
	
	/**
	 * Verifie si la chambre avec les criteres choisis existe
	 * @return
	 */
	public boolean typeChambreParCriteresExist(int indice) {
		return choisirTypeChambreParCriteres(indice) != null;
	}
	
	/**
	 * Retourne le type de chambre selon les criteres choisis par le client sinon null;
	 * @return
	 */
	public TypeChambre choisirTypeChambreParCriteres(int indice) {
		if (1<=indice && indice<=typesChambres.length) {
			return typesChambres[indice-1];
		}
		else {
			return null;
		}
	}
	
	/**
	 * Retourne l'indice du type de chambre selon les criteres choisis par le client.
	 * @return
	 */
	public int choisirIndiceTypeChambreParCriteres() {
		String[] infos = infosTypeChambres();
		int n = infos.length;
		boolean[] dispo = disponibiliteTypeChambres();
		System.out.println("Veuillez choisir le type de chambre que vous souhaitez parmi celles disponbles : ");
		for (int i=0; i<n; i++) {
			if (dispo[i]) {
				System.out.println((i+1)+" -> \n"+infos[i]+"\n");
				
			}
		}
		System.out.print("Votre choix : ");
		int reponse = Question.scannerEntier(in);
		return verifIndiceChambre(reponse-1);
	}
	
	/**
	 * Retourne reponse si la chambre a l'indice reponse est disponible et -1 sinon.
	 * @param reponse
	 * @return
	 */
	public int verifIndiceChambre(int reponse) {
		boolean[] dispo = disponibiliteTypeChambres();
		int n = dispo.length;
		if (0<=reponse && reponse<n && dispo[reponse]) {
			return reponse;
		}
		else {
			System.out.println("Reponse invalide, il n'y a pas de chambre disponible avec ces criteres.");
			return -1;
		}
	}
	
	/**
	 * Retourne le tableau des informations de chaque types de chambres.
	 * @return
	 */
	public String[] infosTypeChambres() {
		String[] res = new String[typesChambres.length];
		for (int i=0; i<res.length; i++) {
			res[i] = "";
		}
		for (int i=0; i<typesChambres.length; i++) {
			String[] infos = typesChambres[i].infosTypeChambre();
			for (int j=0; j<infos.length; j++) {
				res[i] += infos[j]+" ";
			}
		}
		return res;
	}
	
	/**
	 * Retourne un tableau de booleens :
	 * Si un type de chambre est disponible, alors la case correspondante est 
	 * a "true" sinon, a "false".
	 * @return
	 */
	public boolean[] disponibiliteTypeChambres() {
		boolean[] dispo = new boolean[typesChambres.length];
		for (int i=0; i<typesChambres.length; i++) {
			int[] plageChambres = trouverIndicesChambresParTypeChambre(typesChambres[i]);
			int j=0;
			while (j<plageChambres.length && !dispo[i]) {
				if (chambres[plageChambres[j]].chambreLibre()) {
					dispo[i] = true;
				}
				j++;
			}
		}
		return dispo;
	}
	
	/**
	 * Calcule l'indice ou se trouve la chambre dans le tableau de chambres a partir
	 * de l'indice dans le tableau de capaciteTypeChambre.
	 * @param indice
	 * @return
	 */
	public int[] trouverIndicesChambresParTypeChambre(TypeChambre type) {
		int res[] = new int[type.getNbExemplaires()];
		int cpt = 0;
		int i=0;
		while (i<chambres.length && cpt<type.getNbExemplaires()) {
			if (chambres[i].getType().equals(type)) {
				res[cpt] += i;
				cpt++;
			}
			i++;
		}
		return res;
	}
	
	
	
	/**
	 * Retourne la premiere chambre qui est libre si bool = true 
	 * et occupee sinon
	 * S'il ne touve pas, il retourne -1
	 * @param bool
	 * @return
	 */
	public int premiereChambre(boolean bool) {
		int i=0;
		while (i<chambres.length && chambres[i].chambreLibre() != bool) {
			i++;
		}
		if (i == chambres.length) {
			return -1;
		}
		return i;
	}
	
	/**
	 * Affiche la premiere chambre qui est libre si bool = true 
	 * et occupee sinon et indique aussi s'il n'y en a pas.
	 * @param bool
	 */
	public void affichagePremiereChambre(boolean bool) {
		String mot = "";
		if (bool) {
			mot = "vide";
		}
		else {
			mot = "occupee";
		}
		int i = premiereChambre(bool);
		if (i>=0) {
			System.out.println("La premiere chambre "+mot
					+" est la numero "+(i+1));
		}
		else {
			System.out.println("Il n'y a pas de chambre "+mot+".");
		}
	}
	
	/**
	 * Retourne la derniere chambre qui est libre si bool = true et occupee sinon
	 * S'il ne touve pas, il retourne -1
	 * @param bool
	 * @return
	 */
	public int derniereChambre(boolean bool) {
		int i=chambres.length-1;
		while (i>=0 && chambres[i].chambreLibre() != bool) {
			i--;
		}
		if (i == -1) {
			return -1;
		}
		return i;
	}
	
	/**
	 * Affiche la derniere chambre qui est libre si bool = true 
	 * et occupee sinon et indique aussi s'il n'y en a pas.
	 * @param bool
	 */
	public void affichageDerniereChambre(boolean bool) {
		String mot = "";
		if (bool) {
			mot = "vide";
		}
		else {
			mot = "occupee";
		}
		int i = derniereChambre(bool);
		if (i>=0) {
			System.out.println("La derniere chambre "+mot
						+" est la numero "+(i+1));
		}
		else {
			System.out.println("Il n'y a pas de chambre "+mot+".");
		}
	}
	
	/**
	 * Affiche les infos d'une liste de chambres libres
	 * ou occupees passees en parametre.
	 * @param liste
	 * @param libre_reservees
	 */
	public void affichageChambres(Chambre[] liste, String libre_reservees) {
		System.out.println(libre_reservees+"\n");
		for (int i=0; i<liste.length; i++) {
			liste[i].getType().infos();
		}
	}
	
	/**
	 * Retourne la liste des chambres libres de l'hotel.
	 * @return
	 */
	public Chambre[] listeChambresLibres() {
		int nb = nbChambresLibres();
		Chambre[] listeChambres = new Chambre[nb];
		int i=0;
		int j=0;
		while (i<listeChambres.length && j<chambres.length) {
			if (chambres[j].chambreLibre()) {
				listeChambres[i] = chambres[j];
				i++;
			}
			j++;
		}
		return listeChambres;
	}
	
	/**
	 * Retourne la liste des chambres reservees de l'hotel.
	 * @return
	 */
	public Chambre[] listeChambresReservees() {
		int nb = nbChambresReservees();
		Chambre[] listeChambres = new Chambre[nb];
		int i=0;
		int j=0;
		while (i<listeChambres.length && j<chambres.length) {
			if (!chambres[j].chambreLibre()) {
				listeChambres[i] = chambres[j];
				i++;
			}
			j++;
		}
		return listeChambres;
	}
	
	/**
	 * Retourne le nombre de chambres  libres dans l'hotel.
	 * @return
	 */
	public int nbChambresLibres() {
		int res = 0;
		for (int i=0; i<chambres.length; i++) {
			if (chambres[i].chambreLibre()) {
				res++;
			}
		}
		return res;
	}
	
	/**
	 * Affiche le nombre de chambres libres dans l'hotel.
	 */
	public void affichageNbChambresLibres() {
		System.out.println("Il y a "+nbChambresLibres()+" chambres libres dans l'hotel.\n");
	}
	
	/**
	 * Retourne le nombre de chambres reservees dans l'hotel.
	 * @return
	 */
	public int nbChambresReservees() {
		int res = 0;
		for (int i=0; i<chambres.length; i++) {
			if (!chambres[i].chambreLibre()) {
				res++;
			}
		}
		return res;
	}
	
	/**
	 * Affiche le nombre de chambres reservees dans l'hotel.
	 */
	public void affichageNbChambresReservees() {
		System.out.println("Il y a "+nbChambresReservees()+" chambres reservees dans l'hotel.\n");
	}
	
	
	/**
	 * Cree la liste de chambres de l'hotel a partir des
	 * donnes contenues dans typeChambres.
	 * @return
	 */
	private Chambre[] creationHotel() {
		int nb = nbTotalChambres(typesChambres);
		Chambre[] hotel = new Chambre[nb];
		int i=0;
		int j=0;
		while (i<typesChambres.length && j<nb) {
			for (int k=0; k<typesChambres[i].getNbExemplaires(); k++) {
				hotel[j] = new Chambre(typesChambres[i]);
				j++;
			}
			i++;
		}
		return hotel;
	}
	
	/**
	 * Permet de liberer une chambre 
	 */
	public void liberation() {
		affichageMenuAuthentif();
		if (verifMotDePasse()) {
			String choix = "2";
			while (!(choix.equals("0") || choix.equals("1"))) {
				System.out.println(
						"taper 0 pour faire une recherche a partir du nom du client, sinon 1 pour le faire a partir du numero de la chambre");
				choix = in.next();
				in.nextLine();
			}
			if (choix.equals("1")) {
				System.out.println("entrer le numero de la chambre");
				int numeroChambre = Question.scannerEntier(in)-1;
				Chambre type = chambres[numeroChambre];
				int i = 0;
				System.out.println("date de fin de la reservation a annuler");
				LocalDate finInitiale = Question.scannerDate(in);
				while (i <chambres[numeroChambre].getReservations().length) {
					if (chambres[numeroChambre].getReservations()[i]!=null && chambres[numeroChambre].getReservations()[i].getFin().equals(finInitiale)) {
						chambres[numeroChambre].getReservations()[i]=null;
						System.out.println("La reservation a ete retiree.");
						break;
					}
					i++;
				}
			}
			if (choix.equals("0")) {
				System.out.println("entrer le nom du client");
				String trouverNom = in.next();
				in.nextLine();
				System.out.println("entrer prenom du client");
				String trouverPrenom = in.nextLine();
				if (clientExists(trouverNom, trouverPrenom)) {
					int [] e=trouverIndicesChambresParNomPrenom(trouverNom, trouverPrenom);
					System.out.println("liberer quelle chambre ?");
					int a = Question.scannerEntier(in); 	//choix de quelle chambre le client libere parmis celles qu'il a reserv�
					System.out.println("date de fin de la reservation a liberer");
					LocalDate finInitiale = Question.scannerDate(in);
					int nn = 0;
					for (int i = 0; i < e.length; i++) {
						if (a==e[i]) { 		//comparaison entre le num de celle qu'il veut liberer et celle qu'il possede
							nn=a;
						}}
					Chambre type = chambres[nn];
					int j = 0;
					while (j < trouverIndicesReservationParNomPrenom(trouverNom, trouverPrenom).length) {
						if (type.getReservations()[trouverIndicesReservationParNomPrenom(trouverNom, trouverPrenom)[j]].getFin().equals(finInitiale)) {
							chambres[nn].getReservations()[trouverIndicesReservationParNomPrenom(trouverNom, trouverPrenom)[j]]=null;
							System.out.println("La reservation a ete retiree.");
							break;
						}
						j++;
					}
				}
				else System.out.println("Le client n'existe pas.");
			}
		} 
	}
	
	/**
	 * Permet d'effectuer la modification d'une reservation
	 */
	public void modification () {
		affichageMenuAuthentif();
		if (verifMotDePasse()) {
			System.out.println("login client ?");
			String login = in.next();
			in.nextLine();
			int quelReservation =trouverIndiceReservationParLogin(login);
			int quelleChambre = trouverIndiceChambreParLogin(login);
			
			if (quelReservation!=-1 && quelleChambre!=-1) {
				LocalDate ancienDebut =chambres[quelleChambre].getReservations()[quelReservation].getDebut();
				LocalDate ancienneFin =chambres[quelleChambre].getReservations()[quelReservation].getFin();
				int diffDuree = ancienneFin.compareTo(ancienDebut);
			
				System.out.print("Veuillez entrer la nouvelle date pour le debut du sejour : ");
				LocalDate debut = Question.scannerDate(in);
				System.out.print("Veuillez entrer la nouvelle date pour la fin du sejour : ");
				LocalDate fin = Question.scannerDate(in);
				int diff=0;
				if (debut.isBefore(fin)) {
					diff = fin.compareTo(debut);
					chambres[quelleChambre].getReservations()[quelReservation].setDebut(debut);
					chambres[quelleChambre].getReservations()[quelReservation].setFin(fin);
				}
				int resultat = diffDuree - diff;
				boolean bool;
				if (resultat>0) {
					bool=true;
				}
				else bool=false;
				payerFacture(login, bool);
				creationPDFFactureEtEnvoie("CreationsPDFs\\", chambres[quelleChambre].getReservations()[quelReservation], quelleChambre);
			}
			else System.out.println("Login incorrect");
		}
	}
	
	
	
	/**
	 * Calcul le nombre total de chambres a partir des
	 * donnes contenues dans types.
	 * @return
	 */
	public int nbTotalChambres(TypeChambre[] types) {
		int res = 0;
		for (int i=0; i<types.length; i++) {
			res += types[i].getNbExemplaires();
		}
		return res;
	}
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public LocalDate getDateDuJour() {
		return dateDuJour;
	}


	public void setDateDuJour(LocalDate date) {
		dateDuJour = date;
	}

	public Chambre[] getChambres() {
		return chambres;
	}

	public void setChambres(Chambre[] chambres) {
		this.chambres = chambres;
	}

	public TypeChambre[] getTypesChambres() {
		return typesChambres;
	}


	public void setTypesChambres(TypeChambre[] types) {
		typesChambres = types;
	}


	public String[] getEmployes() {
		return employes;
	}

	public void setEmployes(String[] employes) {
		this.employes = employes;
	}
	
	
	
}

package fr.afpa.objets;

import java.time.LocalDate;

import com.itextpdf.layout.element.Paragraph;

import fr.afpa.utilitaire.PDFGenerator;

public class Reservation {

	private String login;
	private Client client;
	private LocalDate debut;
	private LocalDate fin;
	
	
	public Reservation(String login_, Client client_, LocalDate debut_, LocalDate fin_) {
		login = login_;
		client = client_;
		debut = debut_;
		fin = fin_;
	}

	
	/**
	 * Calcule la duree du sejour a partir des dates de debut et de fin
	 * @return
	 */
	public int calculDureeSejour() {
		int i=0;
		LocalDate date = debut;
		while (!date.equals(fin)) {
			date = date.plusDays(1L);
			i++;
		}
		return i;
	}
	
	/**
	 * Retourne une chaine de caracteres avec les infos de 
	 * la reservation.
	 * @return
	 */
	public String infosReservation() {
		String info = "Login : "+login;
		info += "\nDates du sejour : du "+debut+" au "+fin;
		info += "\n"+client.infosClient();
		return info;
	}

	/**
	 * Affichage des infos de la reservation.
	 */
	public void infos() {
		System.out.println(infosReservation());
	}
	
	/**
	 * Creation du fichier PDF qui contiendra la facture
	 * Elle prend en parametre le chemin du dossier qui contiendra le fichier
	 * et la liste des chambres de l'hotel ou reside le client.   
	 * @param dossier
	 * @param chambres
	 */
	public String createInfosPdf(String dossier, int indiceChambre) {
		String fileName = dossier+login+".pdf";
		PDFGenerator.createPDF(fileName, createParagraphe(indiceChambre));
		return fileName;
	}
	
	/**
	 * Creation du paragraphe de la facture pour toutes les reservations pour creer le pdf prend en
	 * parametre la liste des chambres de l'hotel ou reside le client.
	 * @param chambres
	 * @return
	 */
	private Paragraph createParagraphe(int indiceChambre) {
		Paragraph paragraphe = new Paragraph();
		paragraphe.add("Chambre "+(indiceChambre+1)+"\n");
		paragraphe.add(infosReservation());
		return paragraphe;
	}
	
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login_) {
		login = login_;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client_) {
		client = client_;
	}

	public LocalDate getDebut() {
		return debut;
	}

	public void setDebut(LocalDate debut_) {
		debut = debut_;
	}

	public LocalDate getFin() {
		return fin;
	}

	public void setFin(LocalDate fin_) {
		fin = fin_;
	}
	
	
	
	
}

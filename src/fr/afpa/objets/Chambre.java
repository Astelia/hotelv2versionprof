package fr.afpa.objets;

public class Chambre {

	private TypeChambre type;
	private Reservation[] reservations;
	
	
	public Chambre(TypeChambre type_) {
		type = type_;
		reservations = new Reservation[5];
	}

	
	/**
	 * Calcul du cout du sejour
	 * @return
	 */
	public float[] calculCoutSejours() {
		float[] couts = new float[reservations.length];
		for (int i=0; i<couts.length; i++) {
			Reservation reserv = reservations[i];
			if (reserv != null) {
				couts[i] = reserv.calculDureeSejour() * type.getTarif();
			}
		}
		return couts;
	}
	
	/**
	 * Verifie si la chambre peut encore accueuillir une reservation ou non.
	 * @return
	 */
	public boolean chambreLibre() {
		int i=0;
		while (i<reservations.length && reservations[i]!=null) {
			i++;
		}
		return i<reservations.length;
	}
	
	
	public TypeChambre getType() {
		return type;
	}

	public void setType(TypeChambre type_) {
		type = type_;
	}

	public Reservation[] getReservations() {
		return reservations;
	}

	public void setReservations(Reservation[] reservations_) {
		reservations = reservations_;
	}
	
	
	
}

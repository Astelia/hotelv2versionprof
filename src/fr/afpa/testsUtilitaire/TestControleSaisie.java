package fr.afpa.testsUtilitaire;

import java.time.LocalDate;
import fr.afpa.utilitaire.ControleSaisie;

public class TestControleSaisie {

	public static void main(String[] args) {
		
		testTestDigitPasdArret();
		testTestDigitSArrete();
		
		System.out.println();
		
		testIsStringFloatSansPoint();
		testIsStringFloatAvecUnPoint();
		testIsStringFloatAvecPlusieursPoints();
		
		System.out.println();
		
		testIsStringBooleanPourOui();
		testIsStringBooleanPourNon();
		testIsStringBooleanPourAutre();
		
		System.out.println();
		
		testIsStringEntierAvecEntier();
		testIsStringEntierAvecFloat();
		testIsStringEntierAvecLettres();
		
		System.out.println();
		
		testControleSaisieIsStringLoginClientValideTropCourt();
		testControleSaisieIsStringLoginClientValideTropLong();
		testControleSaisieIsStringLoginClientValideAjoutLettre();
		testControleSaisieIsStringLoginClientValide();
		
		System.out.println();
		
		testControleSaisieIsStringLoginEmployeValideMauvaiseLettre();
		testControleSaisieIsStringLoginEmployeValideTropCourt();
		testControleSaisieIsStringLoginEmployeValideTropLong();
		testControleSaisieIsStringLoginEmployeValideCaracteresSpeciaux();
		
		System.out.println();
		
		testControleSaisieIsStringMailValideSansArobaseEtPoint();
		testControleSaisieIsStringMailValideSansArobase();
		testControleSaisieIsStringMailValideSansPoint();
		testControleSaisieIsStringMailValidePlusieursPoint();
		testControleSaisieIsStringMailValideBonMail();
		
		System.out.println();
		
		testControleSaisieIsStringDateValideSansSlash();
		testControleSaisieIsStringDateValideNombreJourIncorrect();
		testControleSaisieIsStringDateValideNombreMoisIncorrect();
		testControleSaisieIsStringDateValideDoubleSlash();
		
		System.out.println();
		
		testControleSaisieIsDureeSejourInvalide();
		testIsPasDansZoneReservationEntre2Dates();
		testIsPasDansZoneReservationALaMemeDateDebut();
		testIsPasDansZoneReservationALaMemeDateFin();
		testIsPasDansZoneReservationPasDansZone();
		
	}
	
	public static void testIsPasDansZoneReservationPasDansZone() {
		LocalDate str1=LocalDate.of(1997, 12, 20);
		LocalDate str2 = LocalDate.of(2000, 01, 01);
		LocalDate strmid = LocalDate.of(2000, 12, 24);
		boolean reponse = ControleSaisie.isPasDansZoneReservation(str1, str2, strmid);
		System.out.print("testIsPasDansZoneReservationPasDansZone : ");
		if(reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testIsPasDansZoneReservationALaMemeDateFin() {
		LocalDate str1=LocalDate.of(1997, 12, 20);
		LocalDate str2 = LocalDate.of(2000, 01, 01);
		LocalDate strmid = LocalDate.of(2000, 01, 01);
		boolean reponse = ControleSaisie.isPasDansZoneReservation(str1, str2, strmid);
		System.out.print("testIsPasDansZoneReservationALaMemeDate : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testIsPasDansZoneReservationALaMemeDateDebut() {
		LocalDate str1=LocalDate.of(1997, 12, 20);
		LocalDate str2 = LocalDate.of(2000, 01, 01);
		LocalDate strmid = LocalDate.of(1997, 12, 20);
		boolean reponse = ControleSaisie.isPasDansZoneReservation(str1, str2, strmid);
		System.out.print("testIsPasDansZoneReservationALaMemeDate : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testIsPasDansZoneReservationEntre2Dates() {
		LocalDate str1=LocalDate.of(1997, 12, 20);
		LocalDate str2 = LocalDate.of(2000, 01, 01);
		LocalDate strmid = LocalDate.of(1999, 06, 10);
		boolean reponse = ControleSaisie.isPasDansZoneReservation(str1, str2, strmid);
		System.out.print("testIsPasDansZoneReservationEntre2Dates : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsDureeSejourInvalide() {
		LocalDate str = LocalDate.of(2000, 01, 01);
		LocalDate str1=LocalDate.of(1997, 12, 20);
		boolean reponse = ControleSaisie.isDureeSejourValide(str, str1);
		System.out.print("testControleSaisieIsDureeSejourInvalide : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringDateValideDoubleSlash() {
		String str = "10//1997";
		boolean reponse = ControleSaisie.isStringDateValide(str);
		System.out.print("testControleSaisieIsStringDateValideDoubleSlash : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringDateValideNombreMoisIncorrect() {
		String str = "10/1002/1997";
		boolean reponse = ControleSaisie.isStringDateValide(str);
		System.out.print("testControleSaisieIsStringDateValideNombreMoisIncorrect : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringDateValideNombreJourIncorrect() {
		String str = "1002/02/1997";
		boolean reponse = ControleSaisie.isStringDateValide(str);
		System.out.print("testControleSaisieIsStringDateValideNombreJourIncorrect : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringDateValideSansSlash() {
		String str = "10021997";
		boolean reponse = ControleSaisie.isStringDateValide(str);
		System.out.print("testControleSaisieIsStringDateValideSansSlash : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringMailValideBonMail() {
		String mail="charles@gmail.fr";
		boolean reponse = ControleSaisie.isStringMailValide(mail);
		System.out.print("testControleSaisieIsStringMailValideBonMail : ");
		if(reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringMailValidePlusieursPoint() {
		String mail="charles@gmail.fr.com";
		boolean reponse = ControleSaisie.isStringMailValide(mail);
		System.out.print("testControleSaisieIsStringMailValidePlusieursPoint : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringMailValideSansPoint() {
		String mail="charles@fr";
		boolean reponse = ControleSaisie.isStringMailValide(mail);
		System.out.print("testControleSaisieIsStringMailValideSansPoint : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringMailValideSansArobase() {
		String mail="charles.fr";
		boolean reponse = ControleSaisie.isStringMailValide(mail);
		System.out.print("testControleSaisieIsStringMailValideSansArobase : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringMailValideSansArobaseEtPoint() {
		String mail="charles";
		boolean reponse = ControleSaisie.isStringMailValide(mail);
		System.out.print("testControleSaisieIsStringMailValideSansArobaseEtPoint : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringLoginEmployeValideCaracteresSpeciaux() {
		String login="GF*+/";
		boolean reponse = ControleSaisie.isStringLoginEmployeValide(login);
		System.out.print("testControleSaisieisStringLoginClientValideCaracteresSpeciaux : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringLoginEmployeValideTropCourt() {
		String login="GF3";
		boolean reponse = ControleSaisie.isStringLoginEmployeValide(login);
		System.out.print("testControleSaisieisStringLoginClientValideTropCourt : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringLoginEmployeValideTropLong() {
		String login="GH12345";
		boolean reponse = ControleSaisie.isStringLoginEmployeValide(login);
		System.out.print("testControleSaisieisStringLoginClientValideTropLong : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringLoginEmployeValideMauvaiseLettre() {
		String login="GF123";
		boolean reponse = ControleSaisie.isStringLoginEmployeValide(login);
		System.out.print("testControleSaisieisStringLoginClientValideMauvaiseLettre : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringLoginClientValide(){
		String login = "1234567891";
		boolean reponse = ControleSaisie.isStringLoginClientValide(login);
		System.out.print("testControleSaisieIsStringLoginClientValide : ");
		if(reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	public static void testControleSaisieIsStringLoginClientValideAjoutLettre() {
		String login = "12345678ab";
		boolean reponse = ControleSaisie.isStringLoginClientValide(login);
		System.out.print("testControleSaisieIsStringLoginClientValideTropLong : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	
	
	public static void testControleSaisieIsStringLoginClientValideTropLong() {
		String login = "1234567891011";
		boolean reponse = ControleSaisie.isStringLoginClientValide(login);
		System.out.print("testControleSaisieIsStringLoginClientValideTropLong : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	
	
	
	public static void testControleSaisieIsStringLoginClientValideTropCourt() {
		String login = "1234567";
		boolean reponse = ControleSaisie.isStringLoginClientValide(login);
		System.out.print("testControleSaisieIsStringLoginClientValideTropCourt : ");
		if(!reponse) {
			System.out.println("ok");
		}
		else System.out.println("ko");
	}
	
	
	public static void testIsStringDateValideMauvaisMois() {
		String date = "2403/1994";
		boolean reponse = ControleSaisie.isStringDateValide(date);
		System.out.print("testIsStringDateValideMauvaisMois : ");
		if (!reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	public static void testIsStringDateValideMauvaisJour() {
		String date = "24/03/1994";
		boolean reponse = ControleSaisie.isStringDateValide(date);
		System.out.print("testIsStringDateValideMauvaisJour : ");
		if (!reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	/**
	 * Teste si la fonction isStringDateValide retourne false si le String 
	 * passe en parametre n'a pas le bon format.
	 */
	public static void testIsStringDateValideMauvaiseLongueur() {
		String date = "2403/1994";
		boolean reponse = ControleSaisie.isStringDateValide(date);
		System.out.print("testIsStringDateValideMauvaiseLongueur : ");
		if (!reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	/**
	 * Teste si la fonction isStringDateValide retourne false si le String 
	 * passe en parametre n'a pas le bon format.
	 */
	public static void testIsStringDateValideMauvaisFormat() {
		String date = "24-03-1994";
		boolean reponse = ControleSaisie.isStringDateValide(date);
		System.out.print("testIsStringDateValideMauvaisFormat : ");
		if (!reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	/**
	 * Teste si la fonction isStringDateValide retourne true si le String 
	 * passe en parametre est une date valide sans zero en plus.
	 */
	public static void testIsStringDateValideBonneDateSansZero() {
		String date = "24/3/1994";
		boolean reponse = ControleSaisie.isStringDateValide(date);
		System.out.print("testIsStringDateValideBonneDateSansZero : ");
		if (reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	/**
	 * Teste si la fonction isStringDateValide retourne true si le String 
	 * passe en parametre est une date valide.
	 */
	public static void testIsStringDateValideBonneDate() {
		String date = "24/03/1994";
		boolean reponse = ControleSaisie.isStringDateValide(date);
		System.out.print("testIsStringDateValideBonneDate : ");
		if (reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	
	
	/**
	 * Teste si la fonction isStringEntier retourne false lorsque le String passe
	 * en parametre n'est pas un entier.
	 */
	public static void testIsStringEntierAvecLettres() {
		String entier = "truc";
		boolean reponse = ControleSaisie.isStringEntier(entier);
		System.out.print("testIsStringEntierAvecLettres : ");
		if (!reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	/**
	 * Teste si la fonction isStringEntier retourne false lorsque le String passe
	 * en parametre est un float.
	 */
	public static void testIsStringEntierAvecFloat() {
		String entier = "24.0";
		boolean reponse = ControleSaisie.isStringEntier(entier);
		System.out.print("testIsStringEntierAvecFloat : ");
		if (!reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	/**
	 * Teste si la fonction isStringEntier retourne true lorsque le String passe
	 * en parametre est un entier.
	 */
	public static void testIsStringEntierAvecEntier() {
		String entier = "24";
		boolean reponse = ControleSaisie.isStringEntier(entier);
		System.out.print("testIsStringEntierAvecEntier : ");
		if (reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	
	
	/**
	 * Teste si la fonction isStringBoolean retourne false lorsque le String passe en
	 * parametre n'est ni oui ni non avec ou sans majuscule(s)
	 */
	public static void testIsStringBooleanPourAutre() {
		String booleen = "bidule";
		boolean reponse = ControleSaisie.isStringBoolean(booleen);
		System.out.print("testIsStringBooleanPourAutre : ");
		if (!reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	/**
	 * Teste si la fonction isStringBoolean retourne true lorsque le String passe en
	 * parametre est non avec ou sans majuscule(s)
	 */
	public static void testIsStringBooleanPourNon() {
		String booleen = "NoN";
		boolean reponse = ControleSaisie.isStringBoolean(booleen);
		System.out.print("testIsStringBooleanPourNon : ");
		if (reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	/**
	 * Teste si la fonction isStringBoolean retourne true lorsque le String passe en
	 * parametre est oui avec ou sans majuscule(s)
	 */
	public static void testIsStringBooleanPourOui() {
		String booleen = "oUi";
		boolean reponse = ControleSaisie.isStringBoolean(booleen);
		System.out.print("testIsStringBooleanPourOui : ");
		if (reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	
	
	/**
	 * Teste si la fonction isStringFloat retourne false s'il y a plusieurs '.'.
	 */
	public static void testIsStringFloatAvecPlusieursPoints() {
		String decimal = "5.0.0";
		boolean reponse = ControleSaisie.isStringFloat(decimal);
		System.out.print("testIsStringFloatAvecPlusieursPoints : ");
		if (!reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	/**
	 * Teste si la fonction isStringFloat retourne true s'il n'y a qu'un seul '.'.
	 */
	public static void testIsStringFloatAvecUnPoint() {
		String decimal = "50.0";
		boolean reponse = ControleSaisie.isStringFloat(decimal);
		System.out.print("testIsStringFloatAvecUnPoint : ");
		if (reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	/**
	 * Teste si la fonction isStringFloat retourne true s'il n'y a pas de '.'.
	 */
	public static void testIsStringFloatSansPoint() {
		String decimal = "500";
		boolean reponse = ControleSaisie.isStringFloat(decimal);
		System.out.print("testIsStringFloatSansPoint : ");
		if (reponse) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
	
	
	/**
	 * Teste si la fonction testDigit continue d'incrementer i si elle ne rencontre que des chiffres.
	 */
	public static void testTestDigitPasdArret() {
		String mot = "3807";
		int i=0;
		i = ControleSaisie.testDigit(mot, i);
		System.out.print("testTestDigitPasDArret : ");
		if (i==mot.length()) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
		
	}
	
	/**
	 * Teste si la fonction testDigit arrete d'incrementer i lorsqu'elle rencontre un caractere
	 * autre qu'un chiffre.
	 */
	public static void testTestDigitSArrete() {
		String mot = "3a80";
		int i=0;
		i = ControleSaisie.testDigit(mot, i);
		System.out.print("testTestDigitSArrete : ");
		if (i==1) {
			System.out.println("OK");
		}
		else {
			System.out.println("KO");
		}
	}
	
}

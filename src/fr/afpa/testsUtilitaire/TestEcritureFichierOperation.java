package fr.afpa.testsUtilitaire;

import java.time.LocalDate;

import fr.afpa.utilitaire.Operation;

public class TestEcritureFichierOperation {

	public static void main(String[] args) {
		
		Operation operation = new Operation(LocalDate.of(1994, 3, 24), true, 500.2f, "13579");
		operation.fichierOperation("TestEcritureFichier\\");

	}

}

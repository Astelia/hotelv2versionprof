package fr.afpa.testsUtilitaire;

import fr.afpa.objets.TypeChambre;
import fr.afpa.utilitaire.LectureFichier;

public class TestLectureFichier {

	public static void main(String[] args) {

		TypeChambre[] type = LectureFichier.traductionFichier("Fichiers_externes\\ListeChambres_V3.csv");
		
		type[0].infos();

	}

}
